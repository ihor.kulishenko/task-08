package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static org.w3c.dom.Node.ELEMENT_NODE;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public List<Flower> parseDocument() {
        try {
            final var builderFactory = DocumentBuilderFactory.newInstance();
            final var documentBuilder = builderFactory.newDocumentBuilder();
            final var document = documentBuilder.parse(xmlFileName);

            final var root = document.getDocumentElement();
            if (!"flowers".equals(root.getTagName())) {
                throw new RuntimeException("wrong tag name: " + root.getTagName());
            }

            final var attributes = root.getAttributes();

            final var childNodes = root.getChildNodes();
            final var numChildren = childNodes.getLength();

            final var result = new ArrayList<Flower>();

            for (int i = 0; i < numChildren; i++) {
                final var childNode = childNodes.item(i);
                if ("flower".equals(childNode.getNodeName()) && childNode.getNodeType() == ELEMENT_NODE) {
                    result.add(parseFlowerNode(childNode));
                }
            }

            return result;
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, String> getNamespaces(String xmlFileName) {
        try {
            final var document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder().parse(xmlFileName);

            final var root = document.getDocumentElement();
            if ("flowers".equals(root.getNodeName())) {
                final var attributes = root.getAttributes();
                final var result = new HashMap<String, String>();

                for (int i = 0; i < attributes.getLength(); i++) {
                    final var item = attributes.item(i);
                    result.put(item.getNodeName(), item.getTextContent());
                }

                return result;
            }

            return Collections.emptyMap();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void saveToXMLFile(String fileName, List<Flower> flowers, Map<String, String> namespaces) {
        // make dom tree
        final Element rootElement;

        try {
            final var document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .newDocument();

            // create root element
            rootElement = document.createElement("flowers");

            // add namespaces
            for (Map.Entry<String, String> entry : namespaces.entrySet()) {
                rootElement.setAttribute(entry.getKey(), entry.getValue());
            }

            for (Flower flower : flowers) {
                final var flowerElement = document.createElement("flower");

                // name
                final var nameElement = document.createElement("name");
                nameElement.appendChild(document.createTextNode(flower.getName()));
                flowerElement.appendChild(nameElement);

                // soil
                final var soilElement = document.createElement("soil");
                soilElement.appendChild(document.createTextNode(flower.getSoil()));
                flowerElement.appendChild(soilElement);

                // origin
                final var originElement = document.createElement("origin");
                originElement.appendChild(document.createTextNode(flower.getOrigin()));
                flowerElement.appendChild(originElement);

                // visual parameters
                flowerElement.appendChild(createVisualParameters(document, flower.getVisualParameters()));

                // growing tips
                flowerElement.appendChild(createGrowingTips(document, flower.getGrowingTips()));

                // multiplying
                final var multiplyingElement = document.createElement("multiplying");
                multiplyingElement.appendChild(document.createTextNode(flower.getMultiplying()));
                flowerElement.appendChild(multiplyingElement);

                rootElement.appendChild(flowerElement);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // write to file
        try (final var writer = new BufferedWriter(new FileWriter(fileName))) {

            final var transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(rootElement), new StreamResult(writer));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Node createVisualParameters(Document document, Flower.VisualParameters vp) {
        final var root = document.createElement("visualParameters");

        // stemColour
        final var stemColour = document.createElement("stemColour");
        stemColour.appendChild(document.createTextNode(vp.getStemColour()));
        root.appendChild(stemColour);

        // leafColour
        final var leafColour = document.createElement("leafColour");
        leafColour.appendChild(document.createTextNode(vp.getLeafColour()));
        root.appendChild(leafColour);

        // aveLenFlower
        final var aveLenFlower = document.createElement("aveLenFlower");
        aveLenFlower.appendChild(document.createTextNode(Integer.toString(vp.getAveLenFlower())));
        aveLenFlower.setAttribute("measure", vp.getAveLenFlowerMeasure());
        root.appendChild(aveLenFlower);

        return root;
    }

    private Node createGrowingTips(Document document, Flower.GrowingTips gt) {
        final var root = document.createElement("growingTips");

        // tempreture
        final var temperature = document.createElement("tempreture");
        temperature.appendChild(document.createTextNode(Integer.toString(gt.getTemperature())));
        temperature.setAttribute("measure", gt.getTemperatureMeasure());
        root.appendChild(temperature);

        // lighting
        final var lighting = document.createElement("lighting");
        lighting.setAttribute("lightRequiring", gt.isLightRequiring() ? "yes" : "no");
        root.appendChild(lighting);

        // watering
        final var watering = document.createElement("watering");
        watering.appendChild(document.createTextNode(Integer.toString(gt.getWatering())));
        watering.setAttribute("measure", gt.getWateringMeasure());
        root.appendChild(watering);

        return root;
    }

    private Flower parseFlowerNode(Node node) {
        final var result = new Flower();

        final var childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            final var nodeItem = childNodes.item(i);

            if (nodeItem.getNodeType() != ELEMENT_NODE) {
                continue;
            }

            switch (nodeItem.getNodeName()) {
                case "name": {
                    result.setName(nodeItem.getTextContent());
                    break;
                }
                case "soil": {
                    result.setSoil(nodeItem.getTextContent());
                    break;
                }

                case "origin": {
                    result.setOrigin(nodeItem.getTextContent());
                    break;
                }

                case "visualParameters": {
                    result.setVisualParameters(parseVisualParameters(nodeItem));
                    break;
                }

                case "growingTips": {
                    result.setGrowingTips(parseGrowingTips(nodeItem));
                    break;
                }

                case "multiplying": {
                    result.setMultiplying(nodeItem.getTextContent());
                }
            }
        }

        return result;
    }

    private Flower.VisualParameters parseVisualParameters(Node node) {
        final var result = new Flower.VisualParameters();

        final var childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            final var nodeItem = childNodes.item(i);
            if (nodeItem.getNodeType() != ELEMENT_NODE) {
                continue;
            }

            switch (nodeItem.getNodeName()) {
                case "stemColour": {
                    result.setStemColour(nodeItem.getTextContent());
                    break;
                }

                case "leafColour": {
                    result.setLeafColour(nodeItem.getTextContent());
                    break;
                }

                case "aveLenFlower": {
                    result.setAveLenFlower(Integer.parseInt(nodeItem.getTextContent()));
                    final var attributes = nodeItem.getAttributes();

                    final var measureAttribute = attributes.getNamedItem("measure");
                    result.setAveLenFlowerMeasure(measureAttribute.getTextContent());

                    break;
                }
            }
        }

        return result;
    }

    private Flower.GrowingTips parseGrowingTips(Node node) {
        final var result = new Flower.GrowingTips();

        final var childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            final var nodeItem = childNodes.item(i);

            if (nodeItem.getNodeType() != ELEMENT_NODE) {
                continue;
            }

            switch (nodeItem.getNodeName()) {
                case "tempreture": {
                    result.setTemperature(Integer.parseInt(nodeItem.getTextContent()));

                    final var attributes = nodeItem.getAttributes();
                    final var measureAttribute = attributes.getNamedItem("measure");
                    result.setTemperatureMeasure(measureAttribute.getTextContent());

                    break;
                }

                case "lighting": {
                    final var attributes = nodeItem.getAttributes();
                    final var lightRequiring = attributes.getNamedItem("lightRequiring");
                    if ("yes".equals(lightRequiring.getTextContent())) {
                        result.setLightRequiring(true);
                    }

                    break;
                }

                case "watering": {
                    result.setWatering(Integer.parseInt(nodeItem.getTextContent()));
                    final var attributes = nodeItem.getAttributes();
                    final var measure = attributes.getNamedItem("measure");
                    result.setWateringMeasure(measure.getTextContent());

                    break;
                }
            }
        }

        return result;
    }
}
