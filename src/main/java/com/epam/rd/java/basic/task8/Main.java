package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;

public class Main {

	public static void main(String[] args) {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		final var flowers = domController.parseDocument();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		flowers.sort(Comparator.comparing(Flower::getName));

		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		final var namespaces = domController.getNamespaces(xmlFileName);
		domController.saveToXMLFile(outputXmlFile, flowers, namespaces);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		final var flowers2 = saxController.parseDocument();

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		flowers2.sort(Comparator.comparing(Flower::getSoil));

		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		final var namespaces2 = saxController.getNamespaces();
		saxController.saveToXMLFile(outputXmlFile, flowers, namespaces2);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		final var flowers3 = staxController.parseDocument();

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		flowers3.sort(Comparator.comparing(Flower::getOrigin));

		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		final var namespaces3 = staxController.getNamespaces();
		staxController.saveToXMLFile(outputXmlFile, flowers3, namespaces3);
	}
}
