package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Controller for SAX parser.
 */
public class SAXController {

    private final String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    private static Map<String, String> convertAttributes(Attributes attributes) {
        final var length = attributes.getLength();
        final var result = new HashMap<String, String>();

        for (int i = 0; i < length; i++) {
            result.put(attributes.getLocalName(i), attributes.getValue(i));
        }
        return result;
    }

    public List<Flower> parseDocument() {
        try {
            final var saxParser = SAXParserFactory.newInstance().newSAXParser();
            final var handler = new FlowersHandler();
            saxParser.parse(xmlFileName, handler);

            return handler.getParseResult();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, String> getNamespaces() {
        try {
            final var saxParser = SAXParserFactory.newInstance().newSAXParser();
            final var handler = new NamespaceHandler();
            saxParser.parse(xmlFileName, handler);

            return handler.getParseResult();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void saveToXMLFile(String outputXmlFile, List<Flower> flowers, Map<String, String> namespaces) {
        try (final var writer = new BufferedWriter(new FileWriter(outputXmlFile))) {

            final var outputFactory = XMLOutputFactory.newInstance();
            final var streamWriter = outputFactory.createXMLStreamWriter(writer);

            streamWriter.writeStartDocument(StandardCharsets.UTF_8.name(), "1.0");
            streamWriter.writeStartElement("flowers");

            for (Map.Entry<String, String> entry : namespaces.entrySet()) {
                streamWriter.writeAttribute(entry.getKey(), entry.getValue());
            }

            for (Flower flower : flowers) {
                streamWriter.writeStartElement("flower");

                // name
                writeElement(streamWriter, "name", flower.getName());
                writeElement(streamWriter, "soil", flower.getSoil());
                writeElement(streamWriter, "origin", flower.getOrigin());

                // visualParameters
                writeVisualParameters(streamWriter, flower.getVisualParameters());

                // growingTips
                writeGrowingTips(streamWriter, flower.getGrowingTips());

                // multiplying
                writeElement(streamWriter, "multiplying", flower.getMultiplying());

                streamWriter.writeEndElement();
            }
            streamWriter.writeEndElement();
            streamWriter.writeEndDocument();

            streamWriter.flush();
            streamWriter.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void writeVisualParameters(XMLStreamWriter writer, Flower.VisualParameters vp) throws XMLStreamException {
        writer.writeStartElement("visualParameters");

        writeElement(writer, "stemColour", vp.getStemColour());
        writeElement(writer, "leafColour", vp.getLeafColour());
        writeElement(writer, "aveLenFlower", Integer.toString(vp.getAveLenFlower()),
                Map.of("measure", vp.getAveLenFlowerMeasure()));

        writer.writeEndElement();
    }

    private void writeGrowingTips(XMLStreamWriter writer, Flower.GrowingTips gp) throws XMLStreamException {
        writer.writeStartElement("growingTips");

        writeElement(writer, "tempreture", Integer.toString(gp.getTemperature()),
                Map.of("measure", gp.getTemperatureMeasure()));

        writeElement(writer, "lighting", "",
                Map.of("lightRequiring", gp.isLightRequiring() ? "yes" : "no"));

        writeElement(writer, "watering", Integer.toString(gp.getWatering()),
                Map.of("measure", gp.getWateringMeasure()));

        writer.writeEndElement();
    }

    private void writeElement(XMLStreamWriter writer, String name, String text) throws XMLStreamException {
        writer.writeStartElement(name);
        writer.writeCharacters(text);
        writer.writeEndElement();
    }

    private void writeElement(XMLStreamWriter writer, String name, String text, Map<String, String> attributes) throws XMLStreamException {
        if (text != null && !text.isEmpty()) {
            writer.writeStartElement(name);
        } else {
            writer.writeEmptyElement(name);
        }

        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            writer.writeAttribute(entry.getKey(), entry.getValue());
        }

        if (text != null && !text.isEmpty()) {
            writer.writeCharacters(text);
            writer.writeEndElement();
        }

    }

    private static class FlowersHandler extends DefaultHandler {

        private final List<Flower> flowers = new ArrayList<>();

        private Flower flower = null;
        private String text = "";

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            if ("flower".equals(qName)) {
                flower = new Flower();
            } else if ("visualParameters".equals(qName)) {
                flower.setVisualParameters(new Flower.VisualParameters());
            } else if ("growingTips".equals(qName)) {
                flower.setGrowingTips(new Flower.GrowingTips());
            } else if ("aveLenFlower".equals(qName)) {
                final var map = convertAttributes(attributes);
                flower.getVisualParameters().setAveLenFlowerMeasure(map.get("measure"));
            } else if ("tempreture".equals(qName)) {
                final var map = convertAttributes(attributes);
                flower.getGrowingTips().setTemperatureMeasure(map.get("measure"));
            } else if ("lighting".equals(qName)) {
                final var map = convertAttributes(attributes);
                flower.getGrowingTips().setLightRequiring("yes".equals(map.get("lightRequiring")));
            } else if ("watering".equals(qName)) {
                final var map = convertAttributes(attributes);
                flower.getGrowingTips().setWateringMeasure(map.get("measure"));
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) {
            text = String.valueOf(Arrays.copyOfRange(ch, start, start + length)).trim();
            // System.out.format("text: %s%n", str.trim());
        }

        @Override
        public void endElement(String uri, String localName, String qName) {

            if ("flower".equals(qName)) {
                flowers.add(flower);
                flower = null;
            } else if ("name".equals(qName)) {
                flower.setName(text);
            } else if ("soil".equals(qName)) {
                flower.setSoil(text);
            } else if ("origin".equals(qName)) {
                flower.setOrigin(text);
            } else if ("stemColour".equals(qName)) {
                flower.getVisualParameters().setStemColour(text);
            } else if ("leafColour".equals(qName)) {
                flower.getVisualParameters().setLeafColour(text);
            } else if ("aveLenFlower".equals(qName)) {
                flower.getVisualParameters().setAveLenFlower(Integer.parseInt(text));
            } else if ("tempreture".equals(qName)) {
                flower.getGrowingTips().setTemperature(Integer.parseInt(text));
            } else if ("watering".equals(qName)) {
                flower.getGrowingTips().setWatering(Integer.parseInt(text));
            } else if ("multiplying".equals(qName)) {
                flower.setMultiplying(text);
            }

            text = "";
        }

        private List<Flower> getParseResult() {
            return flowers;
        }
    }

    private static class NamespaceHandler extends DefaultHandler {
        private Map<String, String> namespaces;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if ("flowers".equals(qName)) {
                namespaces = convertAttributes(attributes);
            }
        }

        public Map<String, String> getParseResult() {
            return namespaces;
        }
    }
}
