package com.epam.rd.java.basic.task8.entity;

import java.util.StringJoiner;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private String multiplying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Flower.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("soil='" + soil + "'")
                .add("origin='" + origin + "'")
                .add("visualParameters=" + visualParameters)
                .add("growingTips=" + growingTips)
                .add("multiplying='" + multiplying + "'")
                .toString();
    }

    public static class VisualParameters {
        private String stemColour;
        private String leafColour;
        private int aveLenFlower;
        private String aveLenFlowerMeasure;

        public String getStemColour() {
            return stemColour;
        }

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public int getAveLenFlower() {
            return aveLenFlower;
        }

        public void setAveLenFlower(int aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
        }

        public String getAveLenFlowerMeasure() {
            return aveLenFlowerMeasure;
        }

        public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
            this.aveLenFlowerMeasure = aveLenFlowerMeasure;
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", VisualParameters.class.getSimpleName() + "[", "]")
                    .add("stemColour='" + stemColour + "'")
                    .add("leafColour='" + leafColour + "'")
                    .add("aveLenFlower=" + aveLenFlower)
                    .add("aveLenFlowerMeasure='" + aveLenFlowerMeasure + "'")
                    .toString();
        }
    }

    public static class GrowingTips {
        private int temperature;
        private String temperatureMeasure;
        private boolean lightRequiring;
        private int watering;
        private String wateringMeasure;

        public int getTemperature() {
            return temperature;
        }

        public void setTemperature(int temperature) {
            this.temperature = temperature;
        }

        public String getTemperatureMeasure() {
            return temperatureMeasure;
        }

        public void setTemperatureMeasure(String temperatureMeasure) {
            this.temperatureMeasure = temperatureMeasure;
        }

        public boolean isLightRequiring() {
            return lightRequiring;
        }

        public void setLightRequiring(boolean lightRequiring) {
            this.lightRequiring = lightRequiring;
        }

        public int getWatering() {
            return watering;
        }

        public void setWatering(int watering) {
            this.watering = watering;
        }

        public String getWateringMeasure() {
            return wateringMeasure;
        }

        public void setWateringMeasure(String wateringMeasure) {
            this.wateringMeasure = wateringMeasure;
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", GrowingTips.class.getSimpleName() + "[", "]")
                    .add("temperature=" + temperature)
                    .add("temperatureMeasure=" + temperatureMeasure)
                    .add("lightRequiring=" + lightRequiring)
                    .add("watering=" + watering)
                    .add("wateringMeasure='" + wateringMeasure + "'")
                    .toString();
        }
    }
}
