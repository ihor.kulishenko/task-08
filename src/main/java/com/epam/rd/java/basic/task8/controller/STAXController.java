package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.events.Attribute;
import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> parseDocument() {

        final var result = new ArrayList<Flower>();

        try (final var reader = new BufferedReader(new FileReader(xmlFileName))) {

            final var eventReader = XMLInputFactory.newInstance().createXMLEventReader(reader);

            Flower flower = new Flower();
            String text = "";
            while (eventReader.hasNext()) {
                final var event = eventReader.nextEvent();

                if (event.isStartElement()) {

                    final var elementName = event.asStartElement().getName().getLocalPart();
                    if ("flower".equals(elementName)) {
                        flower = new Flower();
                    } else if ("visualParameters".equals(elementName)) {
                        flower.setVisualParameters(new Flower.VisualParameters());
                    } else if ("growingTips".equals(elementName)) {
                        flower.setGrowingTips(new Flower.GrowingTips());
                    } else if ("aveLenFlower".equals(elementName)) {
                        final var map = convertAttributes(event.asStartElement().getAttributes());
                        flower.getVisualParameters().setAveLenFlowerMeasure(map.get("measure"));
                    } else if ("tempreture".equals(elementName)) {
                        final var map = convertAttributes(event.asStartElement().getAttributes());
                        flower.getGrowingTips().setTemperatureMeasure(map.get("measure"));
                    } else if ("lighting".equals(elementName)) {
                        final var map = convertAttributes(event.asStartElement().getAttributes());
                        flower.getGrowingTips().setLightRequiring("yes".equals(map.get("lightRequiring")));
                    } else if ("watering".equals(elementName)) {
                        final var map = convertAttributes(event.asStartElement().getAttributes());
                        flower.getGrowingTips().setWateringMeasure(map.get("measure"));
                    }

                } else if (event.isCharacters()) {
                    text = event.asCharacters().getData().trim();

                } else if (event.isEndElement()) {
                    final var elementName = event.asEndElement().getName().getLocalPart();

                    if ("flower".equals(elementName)) {
                        result.add(flower);
                    } else if ("name".equals(elementName)) {
                        flower.setName(text);
                    } else if ("soil".equals(elementName)) {
                        flower.setSoil(text);
                    } else if ("origin".equals(elementName)) {
                        flower.setOrigin(text);
                    } else if ("stemColour".equals(elementName)) {
                        flower.getVisualParameters().setStemColour(text);
                    } else if ("leafColour".equals(elementName)) {
                        flower.getVisualParameters().setLeafColour(text);
                    } else if ("aveLenFlower".equals(elementName)) {
                        flower.getVisualParameters().setAveLenFlower(Integer.parseInt(text));
                    } else if ("tempreture".equals(elementName)) {
                        flower.getGrowingTips().setTemperature(Integer.parseInt(text));
                    } else if ("watering".equals(elementName)) {
                        flower.getGrowingTips().setWatering(Integer.parseInt(text));
                    } else if ("multiplying".equals(elementName)) {
                        flower.setMultiplying(text);
                    }
                }
            }

            return result;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public Map<String, String> getNamespaces() {
        try (final var reader = Files.newBufferedReader(Path.of(xmlFileName))) {
            final var eventReader = XMLInputFactory.newInstance().createXMLEventReader(reader);

            while (eventReader.hasNext()) {
                final var event = eventReader.nextEvent();

                if (event.isStartElement() && "flowers".equals(event.asStartElement().getName().getLocalPart())) {

                    final var result = new HashMap<String, String>();

                    final Function<QName, String> flatterName = (_q) -> _q.getPrefix().isEmpty()
                            ? _q.getLocalPart()
                            : _q.getPrefix() + (_q.getLocalPart().isEmpty() ? "" : ":" + _q.getLocalPart());

                    final var namespaceIterator = event.asStartElement().getNamespaces();
                    while (namespaceIterator.hasNext()) {
                        final var namespace = namespaceIterator.next();
                        result.put(flatterName.apply(namespace.getName()), namespace.getValue());
                    }

                    final var attributeIterator = event.asStartElement().getAttributes();
                    while (attributeIterator.hasNext()) {
                        final var attribute = attributeIterator.next();
                        result.put(flatterName.apply(attribute.getName()), attribute.getValue());
                    }

                    return result;
                }
            }

            return Collections.emptyMap();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void saveToXMLFile(String outputXmlFile, List<Flower> list, Map<String, String> namespaces) {
        try (final var writer = Files.newBufferedWriter(Path.of(outputXmlFile))) {
            final var streamWriter = XMLOutputFactory.newInstance().createXMLEventWriter(writer);

            final var eventFactory = XMLEventFactory.newInstance();

            streamWriter.add(eventFactory.createStartDocument(StandardCharsets.UTF_8.name()));

            // flowers
            final var attributes = namespaces.entrySet().stream()
                    .map(_e -> eventFactory.createAttribute(_e.getKey(), _e.getValue()))
                    .collect(Collectors.toList());

            streamWriter.add(eventFactory.createStartElement(new QName("flowers"), attributes.iterator(), Collections.emptyIterator()));

            for (Flower flower : list) {
                streamWriter.add(eventFactory.createStartElement("", "", "flower"));

                // name
                streamWriter.add(eventFactory.createStartElement("", "", "name"));
                streamWriter.add(eventFactory.createCharacters(flower.getName()));
                streamWriter.add(eventFactory.createEndElement("", "", "name"));

                // soil
                streamWriter.add(eventFactory.createStartElement("", "", "soil"));
                streamWriter.add(eventFactory.createCharacters(flower.getSoil()));
                streamWriter.add(eventFactory.createEndElement("", "", "soil"));

                // origin
                streamWriter.add(eventFactory.createStartElement("", "", "origin"));
                streamWriter.add(eventFactory.createCharacters(flower.getOrigin()));
                streamWriter.add(eventFactory.createEndElement("", "", "origin"));

                // visualParameters
                streamWriter.add(eventFactory.createStartElement("", "", "visualParameters"));

                /// stemColour
                streamWriter.add(eventFactory.createStartElement("", "", "stemColour"));
                streamWriter.add(eventFactory.createCharacters(flower.getVisualParameters().getStemColour()));
                streamWriter.add(eventFactory.createEndElement("", "", "stemColour"));

                /// leafColour
                streamWriter.add(eventFactory.createStartElement("", "", "leafColour"));
                streamWriter.add(eventFactory.createCharacters(flower.getVisualParameters().getLeafColour()));
                streamWriter.add(eventFactory.createEndElement("", "", "leafColour"));

                /// aveLenFlower
                streamWriter.add(eventFactory.createStartElement("", "", "aveLenFlower",
                        List.of(eventFactory.createAttribute("measure", flower.getVisualParameters().getAveLenFlowerMeasure())).iterator(),
                        Collections.emptyIterator()));
                streamWriter.add(eventFactory.createCharacters(Integer.toString(flower.getVisualParameters().getAveLenFlower())));
                streamWriter.add(eventFactory.createEndElement("", "", "aveLenFlower"));

                streamWriter.add(eventFactory.createEndElement("", "", "visualParameters"));

                // growingTips
                streamWriter.add(eventFactory.createStartElement("", "", "growingTips"));

                /// tempreture
                streamWriter.add(eventFactory.createStartElement("", "", "tempreture",
                        List.of(eventFactory.createAttribute("measure", flower.getGrowingTips().getTemperatureMeasure())).iterator(),
                        Collections.emptyIterator()));
                streamWriter.add(eventFactory.createCharacters(
                        Integer.toString(flower.getGrowingTips().getTemperature())));
                streamWriter.add(eventFactory.createEndElement("", "", "tempreture"));

                /// lighting
                streamWriter.add(eventFactory.createStartElement("", "", "lighting",
                        List.of(eventFactory.createAttribute("lightRequiring",
                                flower.getGrowingTips().isLightRequiring() ? "yes" : "no")).iterator(),
                        Collections.emptyIterator()));
                streamWriter.add(eventFactory.createEndElement("", "", "lighting"));

                /// watering
                streamWriter.add(eventFactory.createStartElement("", "", "watering",
                        List.of(eventFactory.createAttribute("measure", flower.getGrowingTips().getWateringMeasure())).iterator(),
                        Collections.emptyIterator()));
                streamWriter.add(eventFactory.createCharacters(
                        Integer.toString(flower.getGrowingTips().getWatering())));
                streamWriter.add(eventFactory.createEndElement("", "", "watering"));

                streamWriter.add(eventFactory.createEndElement("", "", "growingTips"));

                // multiplying
                streamWriter.add(eventFactory.createStartElement("", "", "multiplying"));
                streamWriter.add(eventFactory.createCharacters(flower.getMultiplying()));
                streamWriter.add(eventFactory.createEndElement("", "", "multiplying"));

                streamWriter.add(eventFactory.createEndElement("", "", "flower"));
            }

            streamWriter.add(eventFactory.createEndElement("", "", "flowers"));
            streamWriter.add(eventFactory.createEndDocument());

            streamWriter.flush();
            streamWriter.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Map<String, String> convertAttributes(Iterator<Attribute> iterator) {
        final var result = new HashMap<String, String>();

        while (iterator.hasNext()) {
            final var attribute = iterator.next();
            result.put(attribute.getName().getLocalPart(), attribute.getValue());
        }

        return result;
    }
}
